/*
 * neopixel_stacker.c - Stacker on a 32x8 neopixel array
 *
 * Created: 04.04.2020
 * Author: echoromeo
 */
#include <avr/io.h>
#include <util/delay.h>
#include "libavr_neopixel_spi/neopixel.h"

void neopixel_configure_2darray_oddflip(color_t* array, uint8_t rows, uint8_t columns);

// The full array
#define COLUMNS 32
#define ROWS 8
#define NUM_LEDS (COLUMNS*ROWS)
static color_t data[COLUMNS][ROWS];

// The button
#define BUTTONPORT  PORTC
#define BUTTON1_bm   PIN4_bm
#define BUTTON1CTRL  PIN4CTRL
#define BUTTON2_bm   PIN3_bm
#define BUTTON2CTRL  PIN3CTRL

// The game
#define TIMING_MS       80
#define BRIGHTNESS_FULL 0x1f
#define BRIGHTNESS_LOW  0x10
#define START1MASK      0x0f
#define START2MASK      0xf0

#define INACTIVE        0
#define PLAYING         1
#define LOST            2
#define WIN             0xff

typedef union game_mask {
	struct {
		uint16_t high    : 3;
		uint16_t display : 8;
		uint16_t low     : 3;
	};
	uint16_t full;
} game_mask_t;

typedef struct game {
	uint8_t status;
	uint8_t y;
	game_mask_t current;
	game_mask_t previous;
	uint8_t dir;
	uint8_t debounce;
} game_t;

int main(void)
{
	// Configure clock
#if ((F_CPU == 20000000ul) || (F_CPU == 16000000ul))
	_PROTECTED_WRITE(CLKCTRL.MCLKCTRLB, 0);
#elif ((F_CPU == 10000000ul) || (F_CPU == 8000000ul))
	_PROTECTED_WRITE(CLKCTRL.MCLKCTRLB, CLKCTRL_PDIV_2X_gc | CLKCTRL_PEN_bm);
#endif

	// Init the driver and turn off a lot of LEDs
	neopixel_init();

	// Init the button
	BUTTONPORT.BUTTON1CTRL = PORT_PULLUPEN_bm | PORT_ISC_FALLING_gc;
	BUTTONPORT.BUTTON2CTRL = PORT_PULLUPEN_bm | PORT_ISC_FALLING_gc;

	while(1) {
		// Reset game
		game_t player1 = {
			.status = INACTIVE,
			.y = 0,
			.current = {.display = START1MASK},
			.previous = {.display = 0xff},
			.dir = 1,
			.debounce = 1,
		};
		game_t player2 = {
			.status = INACTIVE,
			.y = COLUMNS - 1,
			.current = {.display = START2MASK},
			.previous = {.display = 0xff},
			.dir = 0,
			.debounce = 1,
		};

		_delay_ms(500);
		neopixel_configure_off(NUM_LEDS);

		// Wait for game start
		BUTTONPORT.INTFLAGS = BUTTON1_bm | BUTTON2_bm;
		while(!(BUTTONPORT.INTFLAGS & (BUTTON1_bm | BUTTON2_bm))) {
			// TODO: Make some cool waiting animation here?
		}
		
		if(BUTTONPORT.INTFLAGS & BUTTON1_bm) {
			player1.status = PLAYING;
			BUTTONPORT.INTFLAGS = BUTTON1_bm;
		}
		if(BUTTONPORT.INTFLAGS & BUTTON2_bm) {
			player2.status = PLAYING;
			BUTTONPORT.INTFLAGS = BUTTON2_bm;
		}

		// Turn off all LEDs before start?
		neopixel_configure_off(NUM_LEDS);
		for(uint8_t i = 0; i < COLUMNS; i++) {
			for(uint8_t j = 0; j < ROWS; j++) {
				data[i][j].channel = 0;
			}
		}
		_delay_ms(500);

		// Let's play!
		uint8_t gamestate = PLAYING;
		while(gamestate == PLAYING) {
			// Update the two active LED rows
			for(uint8_t i = 0; i < ROWS; i++) {
				if(player1.status != INACTIVE) {
					if(player1.current.display & (1 << i)) {
						data[player1.y][i].r = BRIGHTNESS_FULL;
						data[player1.y + 1][i].r = BRIGHTNESS_FULL;
					}
					else {
						data[player1.y][i].r = 0;
						data[player1.y + 1][i].r = 0;
					}
				}
				
				if(player2.status != INACTIVE) {
					if(player2.current.display & (1 << i)) {
						data[player2.y][i].b = BRIGHTNESS_FULL;
						data[player2.y - 1][i].b = BRIGHTNESS_FULL;
					}
					else {
						data[player2.y][i].b = 0;
						data[player2.y - 1][i].b = 0;
					}
				}
			}

			// Configure the entire array
			neopixel_configure_2darray_oddflip((color_t*) &data, ROWS, COLUMNS);

			// The timing of the game
			_delay_ms(TIMING_MS);

			if (player1.status == INACTIVE)  {
				// Player 1 can join if player 2 has not started yet
				if((BUTTONPORT.INTFLAGS & BUTTON1_bm) && (player2.y == COLUMNS - 1)){
					player1.status = PLAYING;
					BUTTONPORT.INTFLAGS = BUTTON1_bm;
				}
			}
			// Player 1 is active
			else {
				// Debounce button
				if(player1.debounce) {
					player1.debounce--;
					BUTTONPORT.INTFLAGS = BUTTON1_bm;
				}

				// Player 1 button was pushed!
				if(BUTTONPORT.INTFLAGS & BUTTON1_bm) {
					BUTTONPORT.INTFLAGS = BUTTON1_bm;
					player1.debounce = 2; // TODO: Need to improve debounce?

					// And together this mask with the previous to figure out if you missed
					player1.current.full &= player1.previous.full;
					player1.previous.full = player1.current.full;

					if(!player1.current.display) {
						// You missed! You lose..
						player1.status = LOST;
					}
					else {
						// Update to remove any non-overlapping LEDs
						for(uint8_t i = 0; i < ROWS; i++) {
							if(player1.previous.display & (1 << i)) {
								data[player1.y][i].r = BRIGHTNESS_FULL;
								data[player1.y + 1][i].r = BRIGHTNESS_FULL;
							}
							else {
								data[player1.y][i].r = 0;
								data[player1.y + 1][i].r = 0;
							}
						}

						// Jump to next two columns
						player1.y += 2;
						if(player1.y >= COLUMNS) {
							player1.status = WIN;
							gamestate = INACTIVE;
						}
					}
				}

				// Move player 1 LEDs
				if(player1.dir) {
					player1.current.full <<= 1;

					// Allow to shift far enough left to get only one LED on
					if(player1.current.display == (1 << 7)) {
						player1.dir = 0;
					}
				}
				else {
					player1.current.full >>= 1;

					// Allow to shift far enough right to get only one LED on
					if(player1.current.display == (1 << 0)) {
						player1.dir = 1;
					}
				}
			}

			if (player2.status == INACTIVE)  {
				// Player 2 can join if player 1 has not started yet
				if((BUTTONPORT.INTFLAGS & BUTTON2_bm) && (player1.y == 0)) {
					player2.status = PLAYING;
					BUTTONPORT.INTFLAGS = BUTTON2_bm;
				}
			}
			// Player 2 is active
			else {
				// Debounce button
				if(player2.debounce) {
					player2.debounce--;
					BUTTONPORT.INTFLAGS = BUTTON2_bm;
				}

				// Player 2 button was pushed!
				if(BUTTONPORT.INTFLAGS & BUTTON2_bm) {
					BUTTONPORT.INTFLAGS = BUTTON2_bm;
					player2.debounce = 2; // TODO: Need to improve debounce?

					// And together this mask with the previous to figure out if you missed
					player2.current.full &= player2.previous.full;
					player2.previous.full = player2.current.full;

					if(!player2.current.display) {
						// You missed! You lose..
						player2.status = LOST;
					}
					else {
						// Update to remove any non-overlapping LEDs
						for(uint8_t i = 0; i < ROWS; i++) {
							if(player2.previous.display & (1 << i)) {
								data[player2.y][i].b = BRIGHTNESS_FULL;
								data[player2.y - 1][i].b = BRIGHTNESS_FULL;
							}
							else {
								data[player2.y][i].b = 0;
								data[player2.y - 1][i].b = 0;
							}
						}

						// Jump to next two columns
						player2.y -= 2;
						if(player2.y >= COLUMNS) {
							player2.status = WIN;
							gamestate = INACTIVE;
						}
					}
				}


				// Move player 2 LEDs
				if(player2.dir) {
					player2.current.full <<= 1;

					// Allow to shift far enough left to get only one LED on
					if(player2.current.display == (1 << 7)) {
						player2.dir = 0;
					}
				}
				else {
					player2.current.full >>= 1;

					// Allow to shift far enough right to get only one LED on
					if(player2.current.display == (1 << 0)) {
						player2.dir = 1;
					}
				}
			}

			// End game if both failed
			if((player1.status == INACTIVE) || (player1.status == LOST)) {
				if((player2.status == INACTIVE) || (player2.status == LOST)) {
					gamestate = INACTIVE;
				}
			}
		}

		// Game over, display win(ner) or fail
		if(player1.status == WIN) {
			// Turn everything around the red stack green
			for(uint8_t i = 0; i < COLUMNS; i++) {
				for(uint8_t j = 0; j < ROWS; j++) {
					if(!data[i][j].r) {
						data[i][j].g = BRIGHTNESS_LOW;
					}
					data[i][j].b = 0;
				}
				neopixel_configure_2darray_oddflip((color_t*) &data, ROWS, COLUMNS);
				_delay_ms(TIMING_MS / 2);
			}
		}
		else if(player2.status == WIN) {
			// Turn everything around the blue stack green
			for(uint8_t i = COLUMNS - 1; i < COLUMNS; i--) {
				for(uint8_t j = 0; j < ROWS; j++) {
					if(!data[i][j].b) {
						data[i][j].g = BRIGHTNESS_LOW;
					}
					data[i][j].r = 0;
				}
				neopixel_configure_2darray_oddflip((color_t*) &data, ROWS, COLUMNS);
				_delay_ms(TIMING_MS / 2);
			}
		}
		else if((player1.status == LOST) || (player2.status == LOST)) {
			if(player2.status == INACTIVE) {
				// Player 1 failed, Turn red from bottom
				for(uint8_t i = 0; i < COLUMNS; i++) {
					for(uint8_t j = 0; j < ROWS; j++) {
						data[i][j].r = BRIGHTNESS_LOW;
					}
					neopixel_configure_2darray_oddflip((color_t*) &data, ROWS, COLUMNS);
					_delay_ms(TIMING_MS / 2);
				}
			}
			else if(player1.status == INACTIVE) {
				// Player 2 failed, Turn red from top
				for(uint8_t i = COLUMNS - 1; i < COLUMNS; i--) {
					for(uint8_t j = 0; j < ROWS; j++) {
						data[i][j].r = BRIGHTNESS_LOW;
					}
					neopixel_configure_2darray_oddflip((color_t*) &data, ROWS, COLUMNS);
					_delay_ms(TIMING_MS / 2);
				}
			}
			else if((player1.status == LOST) && (player2.status == LOST)) {
				// Both failed, turn red from both ends
				for(uint8_t i = 0; i < COLUMNS / 2; i++) {
					for(uint8_t j = 0; j < ROWS; j++) {
						data[i][j].channel = 0;
						data[i][j].r = BRIGHTNESS_LOW;
						data[COLUMNS - 1 - i][j].channel = 0;
						data[COLUMNS - 1 - i][j].r = BRIGHTNESS_LOW;
					}
					neopixel_configure_2darray_oddflip((color_t*) &data, ROWS, COLUMNS);
					_delay_ms(TIMING_MS / 2);
				}
			}
		}
	}
}


void neopixel_configure_2darray_oddflip(color_t* array, uint8_t rows, uint8_t columns)
{
	uint8_t j, i;
	for(i = 0; i < columns; i++) {
		if(i % 2) { //Every odd row flips direction
			for(j = 0; j < rows; j++) {
				neopixel_spi_configure_single(array[(i + 1)*rows - 1 - j]);
			}
		}
		else {
			for(j = 0; j < rows; j++) {
				neopixel_spi_configure_single(array[i * rows + j]);
			}
		}
	}

}
